#!/usr/bin/env python3
'''
Calculate energy eigenvalues for different lattices and plot the bandstructure.
'''

import matplotlib.pyplot as plt
import numpy as np
import pseudopotentials
from bandpath import q_path
from constants import BOHR, HARTREE
from grid_generator import g_vectors, magnitude
from numpy.linalg import eigvalsh


def eigenvalues(vectors, basis, lattice, a, magnitudes, atom):
    '''Calculate a energy matrix for q-points.

    Parameters
    ----------
    vectors : Vectors in an array that sample the q-path [2*pi/a]
    basis : Vectors in an array that form the basis of the hamiltonian [2*pi/a]
    lattice : Lattice type
    a : Lattice constant of the lattice [Angstrom]
    magnitudes : Squared magnitudes of g-vectors with non-zero contribution
    atom : Atom type by its chemical symbol

    Returns
    -------
    Matrix of eigenenergies, columns form energy bands [eV]
    '''
    energies = []
    for i in vectors:
        h = hamiltonian(i, basis, lattice, a, magnitudes, atom)
        energies.append(eigvalsh(h))
    return np.asarray(energies)


def hamiltonian(vector, basis, lattice, a, magnitudes, atom):
    '''Create a Hamiltonian represented as a matrix.

    Parameters
    ----------
    vector : Specific q-vector [2*pi/a]
    basis : Vectors in an array that form the basis of the hamiltonian [2*pi/a]
    lattice : Lattice type
    a : Lattice constant of the lattice [Angstrom]
    magnitudes : Squared magnitudes of g-vectors with non-zero contribution
    atom : Atom type by its chemical symbol

    Returns
    -------
    Hamiltonian as a matrix with potential and kintetic energies [eV]
    '''
    try:
        getattr(pseudopotentials, atom)
    except (AttributeError, TypeError):
        raise SystemExit('ERROR: \'%s\' has to be one of the following: %s' %
                         (atom, ', '.join(pseudopotentials.implemented)))
    basis_size = len(basis)
    matrix = np.zeros((basis_size, basis_size))
    for i in range(basis_size):
        # Add the kinetic energy terms
        matrix[i, i] += magnitude(*(vector + basis[i]), lattice) * \
            (2 * np.pi * BOHR / a)**2 / 2
        for j in range(basis_size):
            tmp = basis[i] - basis[j]
            # If magnitude is one of the non-zero ones add the potential energy
            if magnitude(*tmp, lattice) in magnitudes:
                matrix[i, j] += getattr(pseudopotentials, atom)(tmp, lattice)
    return matrix * HARTREE


def total_bandgap(energies):
    '''Calculate the total bandgap and the fermi level for energy bands.

    Parameters
    ----------
    energies : Matrix of energies in which columns form energy bands [eV]

    Returns
    -------
    bandgap : Highest total bandgap [eV]
    fermi : Fermi level of a semiconductor
    '''
    bandgap = 0
    fermi = 0
    for i in range(len(energies[0]) - 1):
        # Calculate the maximum energy gap between energy bands
        gap = abs(energies[:, i + 1].min()) - abs(energies[:, i].max())
        # Smaller bandgaps than 0.1 eV are most likely numerical errors
        if gap > bandgap and gap > 0.1:
            bandgap = gap
            # Fermi level of a semiconductor lies in the middle of the bandgap
            fermi = abs(energies[:, i].max()) + bandgap / 2
    return bandgap, fermi


def plot_bandstructure(energies, q_sampling, q_special, path, bands, fermi):
    '''Plot energy eigenvalues in a band structure plot.

    Parameters
    ----------
    energies : Matrix of energies in which columns form energy bands [eV]
    q_sampling : Array of lengths of the sampled q-path
    q_special : Array of lengths of all special points
    path : q-path as a string
    bands : Number of energy bands, zero will plot all bands
    fermi : Fermi level
    '''
    plt.figure()
    try:
        if bands <= 0 or bands > len(energies[0]):
            bands = len(energies[0])
            print('WARNING: All energy bands will be plotted!')
        for i in range(bands):
            plt.plot(q_sampling, energies[:, i], '-')
    except TypeError:
        raise SystemExit('ERROR: \'%s\' has to be a positive integer!' % bands)
    e_min = energies[:, 0].min()
    e_max = energies[:, bands - 1].max()
    # Add extra space to the y-axis
    extra = (e_max - e_min) * 0.05
    plt.axis([0, q_sampling[-1], e_min - extra, e_max + extra])
    plt.xlabel('q-path', fontsize=12)
    plt.ylabel('eigenvalues [eV]', fontsize=12)
    for i in set(q_special[:-1]):
        plt.axvline(x=i, c='grey', lw=1)
    plt.axhline(y=fermi, c='grey', lw=1, ls='--')
    # Handle bandpath as tick labels
    i = 0
    tick_name = []
    path_list = list(path)
    while i < len(path_list):
        if path_list[i] == 'G':
            tick_name.append(r'$\Gamma$')
        elif path_list[i] == ',':
            tick_name[-1] = ''.join(path_list[i - 1:i + 2])
            # We added two points to q_special when seperated by a ','
            tick_name.extend(['', ''])
            i += 1
        else:
            tick_name.append(path_list[i])
        i += 1
    plt.xticks(q_special, tick_name)
    plt.tight_layout()
    plt.show()
    return


def bandstructure(atom, lattice, a, path, sampling=100, e_cut=100, bands=10):
    '''Main function to create bandstructures.

    Parameters
    ----------
    atom : Atom type by its chemical symbol
    lattice : Lattice type
    a : Lattice constant of the lattice [Angstrom]
    path : q-path as a string
    sampling : Number of sampling points, defaults to 100
    e_cut : Cut-off energy for g-vectors [eV], defaults to 100
    bands : Number of energy bands, zero will plot all bands, defaults to 10
    '''
    print('Generate g-grid...')
    g_all, mag_all = g_vectors(lattice, a, e_cut)
    print('Generate bandpath...')
    q_points, q_sampling, q_special = q_path(lattice, path, sampling)
    print('Calculate eigenvalues...')
    eigenvals = eigenvalues(q_points, g_all, lattice, a, mag_all, atom)
    bandgap, fermi = total_bandgap(eigenvals)
    print('Total bandgap = %.5f eV' % bandgap)
    plot_bandstructure(eigenvals, q_sampling, q_special, path, bands, fermi)
    return
