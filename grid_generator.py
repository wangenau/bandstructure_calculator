#!/usr/bin/env python3
'''
Calculate g-vectors for different primitive lattice types.
'''

import numpy as np
from constants import BOHR, HARTREE


def g_vectors(lattice, a, e_cut):
    '''Calculate g-vectors and magnitudes of non-zero component g-vectors.

    Parameters
    ----------
    lattice : Lattice type, has to be a key of lattice_condition
    a : Lattice constant [Angstrom]
    e_cut : Cut-off energy for g-vectors [eV]

    Returns
    -------
    g_all : List of all possible g-vectors [2*pi/a]
    mag_all : Sorted list of all unique squared magnitudes non-zero vectors
    '''
    lattice_condition = {'sc': trivial_cond,
                         'bcc': trivial_cond,
                         'fcc': trivial_cond,
                         'diamond': diamond_cond}
    # Represent e_cut in Hartree, then divide the values such that
    # h_bar^2*|G|^2/2*m <= e_cut stays true if we look at |G|^2
    try:
        if a <= 0:
            raise SystemExit('ERROR: \'%s\' has to be larger than zero!' % a)
    except TypeError:
        raise SystemExit('ERROR: \'%s\' has to be a number!' % a)
    tmp = (HARTREE * (2 * np.pi * BOHR / a)**2 / 2)
    try:
        g_cut = e_cut / tmp
    except TypeError:
        raise SystemExit('ERROR: \'%s\' has to be a number!' % e_cut)
    try:
        condition = lattice_condition[lattice]
    except KeyError:
        raise SystemExit('ERROR: \'%s\' has to be one of the following: %s' %
                         (lattice, ', '.join(lattice_condition)))
    g_max = int(g_cut**0.5)
    g_all = []
    mag_all = []
    for h in range(-g_max, g_max + 1):
        for k in range(-g_max, g_max + 1):
            for l in range(-g_max, g_max + 1):
                g_mag = magnitude(h, k, l, lattice)
                if g_mag <= g_cut:
                    g_all.append(np.array([h, k, l]))
                    if condition(h, k, l):
                        mag_all.append(g_mag)
    return g_all, sorted(set(mag_all))


def magnitude(h, k, l, lattice):
    '''Calculate squared magnitudes with respect to the given basis.

    Parameters
    ----------
    h, k, l : Indices that define a vector v = (h, k, l) [2*pi/a]
    lattice : Basis of v, has to be a key of lattice_magnitude

    Returns
    -------
    Squared magnitude for the given vector v by calling a specific function.
    '''
    lattice_magnitude = {'sc': standard_mag,
                         'bcc': bcc_mag,
                         'fcc': fcc_mag,
                         'diamond': fcc_mag}
    try:
        return lattice_magnitude[lattice](h, k, l)
    except KeyError:
        raise SystemExit('ERROR: \'%s\' has to be one of the following: %s'
                         % (lattice, ', '.join(lattice_magnitude)))
    except TypeError:
        raise SystemExit('ERROR: \'%s\', \'%s\', \'%s\' have to be numbers!'
                         % (h, k, l))


def standard_mag(h, k, l):
    '''Calculate the squared magnitude for a given vector in standard basis.'''
    return h**2 + k**2 + l**2


def bcc_mag(h, k, l):
    '''Calculate the squared magnitude for a given vector in bcc basis.'''
    return 2 * standard_mag(h, k, l) + 2 * (h * l + k * l + h * k)


def fcc_mag(h, k, l):
    '''Calculate the squared magnitude for a given vector in fcc basis.'''
    return 3 * standard_mag(h, k, l) - 2 * (h * l + k * l + h * k)


def trivial_cond(h, k, l):
    '''Trivial condition, not more, not less.'''
    return True


def diamond_cond(h, k, l):
    '''Condition for the primitive diamond lattices structure factor to be
    non-zero for a given vector.
    '''
    return (h + k + l) % 4 != 2
