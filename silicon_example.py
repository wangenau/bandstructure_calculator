#!/usr/bin/env python3
'''
Calculate the bandstructure of silicon.
'''

from bandstructure import bandstructure
from constants import SI_CONST


# Necessary parameters
atom = 'Si'          # Atom type
lattice = 'diamond'  # Lattice type
a = SI_CONST         # Lattice constant [Angstrom]
path = 'LGXU,KG'     # Bandpath
# Optional parameters
sampling = 100       # Sample points for the bandpath
e_cut = 60           # Energy cut-off for g-vectors [eV]
bands = 8            # Number of plotted energy bands

bandstructure(atom, lattice, a, path, sampling, e_cut, bands)
