#!/usr/bin/env python3
'''
Collection of constants that are needed throughout the calculation.
'''

# Ha in eV (https://en.wikipedia.org/wiki/Hartree)
HARTREE = 27.211386245988
# Ry in eV
RYDBERG = 0.5 * HARTREE
# a0 in Angstrom (https://en.wikipedia.org/wiki/Bohr_radius)
BOHR = 0.529177210903

# Silicon lattice constant in Angstrom (PhysRevB.10.5095)
SI_CONST = 5.43
# Germanium lattice constant in Angstrom (PhysRev.141.789)
GE_CONST = 5.66
# Tin lattice constant in Angstrom (PhysRev.141.789)
SN_CONST = 6.46
