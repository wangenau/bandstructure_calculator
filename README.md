# bandstructure_calculator

A bandstructure calculator in Python3 with minimal dependencies.

## Dependencies

Recommended package versions
* matplotlib 3.0.3
* numpy 1.18.5

To install them use the command:

    pip3 install -r requirements.txt

## Usage

To learn about the usage of this package, take a look inside [silicon_example.py](silicon_example.py).

The example can be ran with:

    python3 silicon_example.py

It will generate a bandstructure like the following:

![Image: Bandstructure of silicon](silicon_example.png "Bandstructure of silicon")

Currently supported are the materials
* silicon (Si)
* germanium (Ge)
* tin (Sn)

and the lattice types
* simple cubic (sc)
* body-centered cubic (bcc)
* face-centered cubic (fcc)
* diamond (diamond)

To generate other bandstructures one may has to implement new [pseudopotentials](pseudopotentials.py) and/or new lattice types in [grid_generator](grid_generator.py) and [bandpath](bandpath.py).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
