#!/usr/bin/env python3
'''
Pseudopotentials to reproduce bandgaps. Function names have to equal the
chemical symbol of the respective material.
'''

import numpy as np
from constants import HARTREE, RYDBERG
from grid_generator import magnitude

# Keep track of implemented pseudopotentials
implemented = ('Si', 'Ge', 'Sn')


def Si(vector, lattice):
    '''Pseudopotential for silicon. Values taken from PhysRevB.10.5095.'''
    mag = magnitude(*vector, lattice)
    # Compare with PhysRevB.10.5095 Eq. (3) & (4)
    cos = np.cos(np.pi * sum(vector) / 4)
    # We only look at squared magnitudes of non-zero g-vectors
    if mag == 3:
        return -0.2241 * cos * RYDBERG / HARTREE
    if mag == 8:
        return 0.0551 * cos * RYDBERG / HARTREE
    if mag == 11:
        return 0.0724 * cos * RYDBERG / HARTREE
    else:
        return 0


def Ge(vector, lattice):
    '''Pseudopotential for germanium. Values taken from PhysRev.141.789.'''
    mag = magnitude(*vector, lattice)
    cos = np.cos(np.pi * np.sum(vector) / 4)
    if mag == 3:
        return -0.23 * cos * RYDBERG / HARTREE
    if mag == 8:
        return 0.01 * cos * RYDBERG / HARTREE
    if mag == 11:
        return 0.06 * cos * RYDBERG / HARTREE
    else:
        return 0


def Sn(vector, lattice):
    '''Pseudopotential for tin. Values taken from PhysRev.141.789.'''
    mag = magnitude(*vector, lattice)
    cos = np.cos(np.pi * np.sum(vector) / 4)
    if mag == 3:
        return -0.2 * cos * RYDBERG / HARTREE
    if mag == 11:
        return 0.04 * cos * RYDBERG / HARTREE
    else:
        return 0
