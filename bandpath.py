#!/usr/bin/env python3
'''
Sample bandpaths through different lattice types.
'''

import numpy as np
from numpy.linalg import norm

# Values taken from https://lampx.tugraz.at/~hadley/ss1/bzones/
special_points = {'sc': {'G': np.array([0, 0, 0]),
                         'R': np.array([1 / 2, 1 / 2, 1 / 2]),
                         'X': np.array([0, 1 / 2, 0]),
                         'M': np.array([1 / 2, 1 / 2, 0])},
                  'bcc': {'G': np.array([0, 0, 0]),
                          'H': np.array([-1 / 2, 1 / 2, 1 / 2]),
                          'P': np.array([1 / 4, 1 / 4, 1 / 4]),
                          'N': np.array([0, 1 / 2, 0])},
                  'fcc': {'G': np.array([0, 0, 0]),
                          'X': np.array([0, 1 / 2, 1 / 2]),
                          'L': np.array([1 / 2, 1 / 2, 1 / 2]),
                          'W': np.array([1 / 4, 3 / 4, 1 / 2]),
                          'U': np.array([1 / 4, 5 / 8, 5 / 8]),
                          'K': np.array([3 / 8, 3 / 4, 3 / 8])},
                  'diamond': {'G': np.array([0, 0, 0]),
                              'X': np.array([0, 1 / 2, 1 / 2]),
                              'L': np.array([1 / 2, 1 / 2, 1 / 2]),
                              'W': np.array([1 / 4, 3 / 4, 1 / 2]),
                              'U': np.array([1 / 4, 5 / 8, 5 / 8]),
                              'K': np.array([3 / 8, 3 / 4, 3 / 8])}}


def q_path(lattice, path, sampling):
    '''Generate sampled q-paths.

    Parameters
    ----------
    lattice : Lattice type
    path : q-path as a string
    sampling : Number of sampling points

    Returns
    -------
    q_points : Coordinates of sampled q-points [2*pi/a]
    q_sampling : Array of lengths of the sampled q-points
    q_special : Array of lengths of all special points (including ',')
    '''
    try:
        if sampling != int(sampling):
            raise SystemExit('ERROR: \'%s\' has to be an integer!' % sampling)
    except ValueError:
        raise SystemExit('ERROR: \'%s\' has to be a number!' % sampling)
    # Convert path to a list
    points = []
    points[:0] = path.upper()
    if points[0] == ',' or points[-1] == ',':
        raise SystemExit('ERROR: \'%s\' cannot start or end with \',\'!' %
                         path)
    if sampling < len(points):
        print('WARNING: Raise sampling to %s!' % len(points))
        sampling = len(points)
    try:
        s_points = special_points[lattice]
    except KeyError:
        raise SystemExit('ERROR: \'%s\' has to be one of the following: %s' %
                         (lattice, ', '.join(special_points)))
    # Calculate distances between special points in a bandpath
    dists = []
    for i in range(len(points) - 1):
        if points[i] != ',' and points[i + 1] != ',':
            try:
                dist = s_points[points[i + 1]] - s_points[points[i]]
                dists.append(norm(dist))
            except KeyError:
                raise SystemExit(
                    'ERROR: \'%s\' has to be one of the following: %s' %
                    (points[i + 1], ', '.join(s_points)))
        else:
            # No distance when jumping between special points
            dists.append(0)
    # Calculate sample points between special points
    samplings = []
    for i in dists:
        samplings.append(int(round(sampling * i / sum(dists))))
    # Generate q-point coordinates
    q_points = []
    q_points.append(s_points[points[0]])
    for i in range(len(points) - 1):
        if points[i] != ',' and points[i + 1] != ',':
            for n in range(samplings[i]):
                dist = s_points[points[i + 1]] - s_points[points[i]]
                dist = s_points[points[i]] + dist * (n + 1) / samplings[i]
                q_points.append(dist)
    # Calculate distance for each point to plot them over these distances
    q_special = [0]
    q_sampling = [0]
    for i in range(len(dists)):
        for j in range(samplings[i]):
            # Catch potential division by zero
            if samplings[i] != 0:
                q_sampling.append(q_sampling[-1] + dists[i] / samplings[i])
        # Append each special point distance (even for ',')
        q_special.append(q_sampling[-1])
    return q_points, q_sampling, q_special
